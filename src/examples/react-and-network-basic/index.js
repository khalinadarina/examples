/* Core */
import { render } from 'react-dom';

/* Instruments */
import Example from './1';

render(<Example />, document.getElementById('root'));
